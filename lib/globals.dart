import 'package:jovial_svg/jovial_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class Globals {
  static ScalableImageCache svgCache = ScalableImageCache(size: 40);

  static int cityId = 12;
  static String cityName = "Αθήνα - Κέντρο";

  static String currentTheme = "system";

  static SharedPreferences? prefs;
}