import 'package:flutter/material.dart';
import 'package:jovial_svg/jovial_svg.dart';
import 'package:meteoweather/city_chooser.dart';
import 'package:meteoweather/globals.dart';

class SettingsView extends StatefulWidget {
  @override
  State<SettingsView> createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Image.asset("assets/app_icon.png")
        ),
        const Center(child: Text("Meteo Weather Greece"),),

        Expanded(
          child: ListView(
            children: [
              ListTile(
                title: const Text("Select Region"),
                onTap: () async {
                  List<dynamic> result = await Navigator.push(context, MaterialPageRoute(builder: (context) => CityChooserView()));

                  Globals.cityName = result[0];
                  Globals.cityId = result[1];

                  Globals.prefs!.setInt("cityId", Globals.cityId);
                  Globals.prefs!.setString("cityName", Globals.cityName);
                },
                leading: const Icon(Icons.list),
              ),
              ListTile(
                leading: const Icon(Icons.format_paint),
                title: const Text("App Theme"),
                trailing: DropdownMenu(
                  initialSelection: Globals.currentTheme,
                  dropdownMenuEntries: const [
                    DropdownMenuEntry(value: 'system', label: "System"),
                    DropdownMenuEntry(value: 'light', label: "Light"),
                    DropdownMenuEntry(value: 'dark', label: "Dark")
                  ],
                  requestFocusOnTap: true,
                  enableFilter: false,
                  onSelected: (label) {
                    setState(() {
                      Globals.currentTheme = label!;
                      Globals.prefs!.setString("theme", label);
                    });
                  },
                  enableSearch: false,
                ),
              )
            ]
          )
        ),
        const Spacer(),
        Center(
          child: Column(
            children: [
              ScalableImageWidget.fromSISource(si: ScalableImageSource.fromSvg(DefaultAssetBundle.of(context), "assets/noa_logo.svg")),
              const Padding(
                padding: EdgeInsets.all(16.0),
                child: Text("All data are taken from meteo.gr (Εθνικό Αστεροσκοπείο Αθηνών/meteo.gr)", softWrap: true,)
              )
            ],
          )
        ),
        const Center(
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Text("\"Meteo Weather Greece\" is not affiliated with meteo.gr or National Observatory of Athens (Εθνικό Αστεροσκοπείο Αθηνών)", softWrap: true,)
          ),
        )
      ]
    );
  }
}