import 'dart:io';

import 'package:html/dom.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:html/parser.dart';

Future<Map<String, dynamic>> getData(int cityId) async {
  String source = await _getHtmlSource(cityId);
  Document doc = parse(source);

  String temp = _getTemperature(doc);
  String status = _getStatus(doc);
  String humidity = _getHumidityPercentage(doc);
  String pressure = _getPressure(doc);
  String windSpeed = _getWindSpeed(doc);
  return {
    "temp": temp,
    "status": status,
    "humidity": humidity,
    "pressure": pressure,
    "windSpeed": windSpeed
  };
}

Future<List<Map<String, dynamic>>> getForecastEntries(int cityId) async {
  String source = await _getHtmlSource(cityId);
  Document doc = parse(source);

  List<Element> entries = doc.getElementById("outerTable")!.children[0].children;
  String currentDay = "";

  List<Map<String, dynamic>> result = [];

  for (Element element in entries) {
    if (element.className == "") {
      currentDay = element.children[0].getElementsByClassName("dayNumbercf")[0].firstChild!.text!;
      currentDay+=" ";
      currentDay += element.children[0].getElementsByClassName("monthNumbercf")[0].text;
    }

    else if (element.className == "perhour rowmargin") {
      String temp = element.getElementsByClassName("temperature")[0].children[0].firstChild!.text!;
      String time = element.getElementsByClassName("fulltime")[0].text;
      String status = element.getElementsByClassName("phenomeno-name")[0].firstChild!.text!;
      String statusImg = element.getElementsByClassName("phenomeno-name")[0].parent!.getElementsByClassName("CFicon")[0].attributes["src"]!;

      result.add({
        "date": currentDay.replaceAll('\n', ''),
        "temp": temp.replaceAll('\n', '') + "°C",
        "time": time.replaceAll('\n', ''),
        "status": status.replaceAll('\n', ''),
        "statusImg": Uri.https("meteo.gr", statusImg).toString()
      });
    }
  }

  return result;
}

String _getWindSpeed(Document doc) {
  List<Element> windEl = doc.getElementsByClassName("windnr");
  if (windEl.isEmpty) return "N/A";
  return windEl[1].text.replaceAll('\n', '');
}

String _getTemperature(Document doc){
  List<Element> tempEl = doc.getElementsByClassName("newtemp");
  if (tempEl.isEmpty) return "N/A";
  return tempEl[0].text.replaceAll('\n', '');
}

String _getStatus(Document doc) {
  List<Element> iconEl = doc.getElementsByClassName("CFicon");
  if (iconEl.isEmpty) return "N/A";
  return iconEl[0].attributes["title"]!;
}

String _getHumidityPercentage(Document doc) {
  List<Element> humidityEl = doc.getElementsByClassName("ygrasia");
  if (humidityEl.isEmpty) return "N/A";

  String humidityText = humidityEl[0].text;
  return humidityText.replaceAll("Υγρασία:", "").replaceAll('\n', '');
}

String _getPressure(Document doc) {
  List<Element> pressureEl = doc.getElementsByClassName("piesi");
  if (pressureEl.isEmpty) return "N/A";

  String humidityText = pressureEl[0].text;
  return humidityText.replaceAll("Πίεση:", "").replaceAll('\n', '');
}

Future<String> _getHtmlSource(int cityId) async {
  Uri url = Uri.https("meteo.gr", "cf.cfm", {"city_id": cityId.toString()});
  File response = await DefaultCacheManager().getSingleFile(url.toString(),key: "html$cityId");
  return await response.readAsString();
}