import 'package:flutter/material.dart';
import 'package:csv/csv.dart';

class CityChooserView extends StatelessWidget {
  List<List<dynamic>>? cachedData;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(future: loadCityIds(context), builder: (context, snapshot) {
      bool hasFinished = snapshot.hasData && snapshot.connectionState == ConnectionState.done;

      if (!hasFinished) return const Center(child: CircularProgressIndicator());

      if (hasFinished) cachedData = snapshot.data;

      return Scaffold(
        body: ListView.builder(itemBuilder: (context, index) {
          return listBuilder(context, index, cachedData!);
        })
      );
    });
  }

  Future<List<List<dynamic>>> loadCityIds(BuildContext context) async {
    String csvString = await DefaultAssetBundle.of(context).loadString("assets/cityIds.csv");
    return const CsvToListConverter().convert(csvString, eol: '\n');
  }

  Widget? listBuilder(BuildContext context, int index, List<List<dynamic>> data) {
    if (index >= data.length) return null;
    return ListTile(
      title: Text(data[index][0]),
      onTap: () {
        Navigator.pop(context, data[index]);
      }
    );
  }
}