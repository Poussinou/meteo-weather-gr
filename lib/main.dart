import 'package:flutter/material.dart';
import 'package:meteoweather/details.dart';
import 'package:meteoweather/forecasts.dart';
import 'package:meteoweather/globals.dart';
import 'package:meteoweather/settings.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MainApp());

class MainApp extends StatefulWidget {
  @override
  State<MainApp> createState() => _MainAppState();
}
class _MainAppState extends State<MainApp> {
  int currentPageIndex = 0;

  Future<void> initSharedPreferences() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Globals.prefs = sharedPreferences;
    String? theme = Globals.prefs!.getString("theme");
    int? cityId = Globals.prefs!.getInt("cityId");
    String? cityName = Globals.prefs!.getString("cityName");

    print(cityId);
    print(cityName);

    setState(() {
      Globals.currentTheme = theme ?? "system";
      Globals.cityId = cityId ?? 12;
      Globals.cityName = cityName ?? "Αθήνα - Κέντρο";
    });
  }

  @override
  void initState() {
    super.initState();
    initSharedPreferences();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark
      ),

      themeMode: Globals.currentTheme == "system" ? ThemeMode.system :
                  (Globals.currentTheme == "light" ? ThemeMode.light : ThemeMode.dark),
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Meteo Weather"),
          actions: [
            Text(Globals.cityName)
          ],
        ),

        body: [
          DetailedView(),
          ForecastView(),
          SettingsView()
        ][currentPageIndex],

        bottomNavigationBar: NavigationBar(
          selectedIndex: currentPageIndex,
          destinations: const [
            NavigationDestination(
              selectedIcon: Icon(Icons.cloud),
              icon: Icon(Icons.cloud_outlined),
              label: "Weather"
            ),
            NavigationDestination(
              selectedIcon: Icon(Icons.arrow_forward),
              icon: Icon(Icons.arrow_forward_outlined),
              label: "Forecast"
            ),
            NavigationDestination(
              selectedIcon: Icon(Icons.settings),
              icon: Icon(Icons.settings_outlined),
              label: "Settings"
            )
          ],

          onDestinationSelected: (index) {
            setState(() {
              currentPageIndex = index;
            });
          },
        ),
      ),
    );
  }
}