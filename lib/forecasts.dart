import 'package:flutter/material.dart';
import 'package:jovial_svg/jovial_svg.dart';
import 'package:meteoweather/data.dart';
import 'package:meteoweather/globals.dart';

class ForecastView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getForecastEntries(Globals.cityId),
      builder: (context, snapshot) {
        bool hasFinished = snapshot.hasData && snapshot.connectionState == ConnectionState.done;

        if (!hasFinished) {
          return const Center(child: CircularProgressIndicator());
        }

        return ListView.builder(
          itemBuilder: (context, index) {
            return listBuilder(context, index, snapshot.data!);
          },
        );
      }
    );
  }

  Widget? listBuilder(BuildContext context, int index, List<Map<String, dynamic>> data) {
    if (index >= data.length) return null;
    return _ForecastCard(data[index]["date"], data[index]["time"], data[index]["temp"], data[index]["status"], data[index]["statusImg"]);
  }
}

class _ForecastCard extends StatelessWidget {
  final String date;
  final String time;
  final String temp;
  final String status;
  final String statusImg;

  const _ForecastCard(this.date, this.time, this.temp, this.status, this.statusImg);

  final TextStyle textStyle = const TextStyle(fontSize: 28);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100,
        child: Card.filled(
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Row(
              children: [
                const Icon(Icons.date_range),
                Text(date, style: textStyle),
                const SizedBox(width: 50,),
                const Icon(Icons.schedule),
                Text(time, style: textStyle),
                const SizedBox(width: 50,),
                const Icon(Icons.thermostat),
                Text(temp, style: textStyle),
                const SizedBox(width: 50),
                Text(status, style: textStyle),
                ScalableImageWidget.fromSISource(
                  si: ScalableImageSource.fromSvgHttpUrl(Uri.parse(statusImg)),
                  cache: Globals.svgCache
                )
              ],
          )
        )
      )
    );
  }
}