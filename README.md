# Meteo Weather Greece
Meteo Weather Greece is a lightweight, beautiful weather app that fetches data from [meteo.gr](https://meteo.gr)

# Screenshots
![Screenshot of the Weather tab in the app (Dark theme)](metadata/en-US/images/phoneScreenshots/1.png)

![Screenshot of the Settings tab in the app (Light theme)](metadata/en-US/images/phoneScreenshots/6.png)